# pylint: disable=missing-module-docstring,missing-function-docstring

import pytest

import wackler.machine_model.command as mm


@pytest.mark.parametrize(
    "expression, expected",
    (
        [
            'PLC["0:Objects/3:Part/3:Value"]',
            {"source_name": "PLC", "path": "0:Objects/3:Part/3:Value"},
        ],
        [
            'PLC["0:Objects/3:Part/3:Value"][0]',
            {
                "source_name": "PLC",
                "path": "0:Objects/3:Part/3:Value",
                "array_index": 0,
            },
        ],
        [
            'bool(PLC["0:Objects/3:Part/3:Value"][0])',
            {
                "source_name": "PLC",
                "path": "0:Objects/3:Part/3:Value",
                "array_index": 0,
                "type_info": {"data_type": "bool"},
            },
        ],
        [
            'bool(PLC["0:Objects/3:Part/3:Value"][0]) / 1000',
            {
                "source_name": "PLC",
                "path": "0:Objects/3:Part/3:Value",
                "array_index": 0,
                "type_info": {"data_type": "bool"},
                "adjust": {"op": "__mul__", "value": 1000},
            },
        ],
        [
            'PLC["0:Objects/3:Part/3:Value"] / 1000',
            {
                "source_name": "PLC",
                "path": "0:Objects/3:Part/3:Value",
                "adjust": {"op": "__mul__", "value": 1000},
            },
        ],
        [
            'PLC["0:Objects/3:Part/3:Value"][0] / 1000',
            {
                "source_name": "PLC",
                "path": "0:Objects/3:Part/3:Value",
                "array_index": 0,
                "adjust": {"op": "__mul__", "value": 1000},
            },
        ],
        [
            'PLC["0:Objects/3:Part/3:Value"] or PLC["0:Objects/3:Part/3:Value"]',
            {"source_name": "PLC", "path": "0:Objects/3:Part/3:Value"},
        ],
        [
            'PLC["0:Objects/3:Part/3:Value"] and PLC["0:Objects/3:Part/3:Value"]',
            {"source_name": "PLC", "path": "0:Objects/3:Part/3:Value"},
        ],
    ),
)
def test_parse_node_expression(expression, expected):
    node = {
        "name": "fake",
        "expression": expression,
    }
    result = mm.parse_node_expression(node)
    assert result == expected
