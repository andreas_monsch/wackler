# pylint: disable=too-many-arguments,missing-function-docstring

import ast
import logging

logger = logging.getLogger("wackler")


def _parse_subscript(subscript):
    """ Parse array and item subscriptions """
    result = {}
    try:
        # subscription on subscription, assume array access
        if isinstance(subscript.value, ast.Subscript):
            result = _parse_subscript(subscript.value)
            assert isinstance(subscript.slice, ast.Index)
            assert isinstance(subscript.slice.value, ast.Constant)
            result["array_index"] = subscript.slice.value.value
        elif isinstance(subscript.value, ast.Name):
            result["source_name"] = subscript.value.id
            assert isinstance(subscript.slice, ast.Index)
            assert isinstance(subscript.slice.value, ast.Constant)
            result["path"] = subscript.slice.value.value
    except AttributeError:
        return {}

    return result


def _parse_call(call):
    """ Handle function calls in node expressions."""
    # Only bool() calls with a single argument are allowed
    if not all(
        [isinstance(call.func, ast.Name), call.func.id == "bool", len(call.args) == 1]
    ):
        raise ValueError()

    result = {"type_info": {"data_type": "bool"}, **_parse(call.args[0])}
    return result


def _parse_binary_operation(binop):
    """ Parse binary operations

    Note: The function scope is extremely limited, only +,-,*,/ are supported
          and only with constants as right-hand side operands, e.g.
          1. foo * 123,
          2. abc[0] / 100
    """
    if isinstance(binop.right, ast.Constant):
        value = binop.right.value
    else:
        raise ValueError(
            "Only binary operations with a right-hand side constant operand are supported"
        )

    if isinstance(binop.op, ast.Div):
        result = {"adjust": {"op": "__mul__", "value": value}}
    elif isinstance(binop.op, ast.Mult):
        result = {"adjust": {"op": "__truediv__", "value": value}}
    elif isinstance(binop.op, ast.Add):
        result = {"adjust": {"op": "__sub__", "value": value}}
    elif isinstance(binop.op, ast.Sub):
        result = {"adjust": {"op": "__add__", "value": value}}

    result.update(**_parse(binop.left))
    return result


def _parse(expr):
    """ Impl function for node expression parsing"""
    result = {}
    if isinstance(expr, ast.Subscript):
        result = _parse_subscript(expr)
    elif isinstance(expr, ast.Call):
        result = _parse_call(expr)
    elif isinstance(expr, ast.BinOp):
        result = _parse_binary_operation(expr)
    elif isinstance(expr, ast.BinOp):
        result = _parse_binary_operation(expr)
    elif isinstance(expr, ast.BoolOp):
        # Boolean operations are ignored and we simply pick the first operand
        # as the main expr to evaluate.
        result = _parse(expr.values[0])
    else:
        raise ValueError()

    return result


def parse_node_expression(node):
    exprstr = node.get("expression")
    if not exprstr:
        logger.info("Node %r has no expression associated with it", node["name"])
        return None

    try:
        module = ast.parse(exprstr)
        expr = module.body[0]
        if not isinstance(expr, ast.Expr):
            raise ValueError()
        result = _parse(expr.value)
    except (SyntaxError, ValueError):
        logger.error(
            "Node %r has an unsupported expression %r associated with it",
            node["name"],
            exprstr,
            # exc_info=True,
        )
        return None

    return result
