# pylint: disable=too-many-arguments,missing-function-docstring
""" Node handling utilities module """

import json
import logging
import random
import collections

import faker

from wackler.machine_model.parse import parse_node_expression

fake = faker.Faker()
logger = logging.getLogger("wackler")


def normalize_nodes(nodes):
    """ Keep only the relevant node information and drop the rest """
    for node in nodes:
        yield {
            "name": node["name"],
            "expression": node["expression"],
            "type_info": node["type_info"],
            "value_range": node.get("value_range", None),
            **node["meta"],
        }


def extract_path_info(node_path_str):
    """ Try to extract `dirname` and `basename` from `node_path_str`

    This function assumes that the `node_path_str` is a vlaid OPCUA
    browse path, otherwise a `ValueError` is raised.
    """
    path = node_path_str.split("/")
    *dirname, basename = path
    try:
        if not dirname:
            raise ValueError
        _, _ = dirname[0].split(":", 1)
    except ValueError:
        return None
    return dirname, basename


def create_meta_data(model_nodes):
    """ Create create all the meta data required for opcua tree creation and
    variable value simulation

    Ignore nodes with unsupported expressions and invalid paths.
    """
    for node in model_nodes:
        # Only consider nodes with supported expressions
        node_info = parse_node_expression(node)
        if not node_info:
            logger.info("Skipping node %r", node["name"])
            continue

        # Only consider nodes with a valid OPCUA browse paths, otherwise the
        # node may potentially refere to a non OPCUA data source
        path_info = extract_path_info(node_info["path"])
        if not path_info:
            logger.error(
                (
                    "Invalid node path"
                    " - node expression %s probably doesn't refer a OPCUA data source"
                ),
                node["expression"],
            )
            continue

        # Filter Objects from dirname, we always start appending nodes at
        # "0:Objects", so there is no need to make it part of the dirname
        dirname, basename = path_info
        if dirname[0] == "0:Objects":
            dirname = dirname[1:]

        # `cond`s are usually converted to boolean via their expression, so we
        # simply copy the bool cast over as node type
        type_info = node_info.pop("type_info", None)
        if type_info:
            node["type_info"] = type_info

        node["meta"] = {**node_info, "dirname": dirname, "basename": basename}
        yield node


def _split_array_nodes(candidates):
    """ Split a list of nodes into nodes belonging to an array, so called array
    nodes, and nodes referred multiple times in the model which we also refer to
    as 'duplicates'
    """
    array_nodes, duplicates = [], []
    for cand in candidates:
        if cand.get("array_index") is not None:
            array_nodes.append(cand)
        else:
            duplicates.append(cand)
    return array_nodes, duplicates


def _normalize_array_nodes(array_nodes):
    """ Transform a given list of associated `array_nodes` into a single node,
    used for simulating array values.
    """
    array_nodes.sort(key=lambda x: x["array_index"])
    length = array_nodes[-1]["array_index"] + 1
    dct = {node["array_index"]: node for node in array_nodes}

    nodes = []
    node = array_nodes[0].copy()
    for i in range(length):
        curr = dct.get(i)
        if curr:
            nodes.append(curr)
            node = curr.copy()
        else:
            node["array_index"] = i
            nodes.append(node)

    return {"array": {"length": length, "nodes": nodes}}


def group_nodes_by_path(nodes):
    """ Group nodes by their path and aggregate array nodes to make them look
    like a single node. """
    grouped = collections.defaultdict(list)
    for node in nodes:
        grouped[node["path"]].append(node)

    for path, group in grouped.items():
        # Unlistify simple nodes
        if len(group) == 1 and not group[0].get("array_index"):
            grouped[path] = group[0]
            continue

        array_nodes, duplicates = _split_array_nodes(group)
        if array_nodes and duplicates:
            raise ValueError(
                "Some expressions refer to the node path as an array and as a"
                " constant - this is not allowed."
            )

        # Aggregate array nodes and enrich them with array specific meta data
        if array_nodes:
            grouped[path] = _normalize_array_nodes(array_nodes)
        # Make sure to simulate non-array nodes that are referred multiple times from b
        # different nodes only once, by simply picking the first reference
        elif duplicates:
            logger.debug("Path %r refered multiple times from different nodes", path)
            logger.debug(
                "Detailed duplicate listing:\n %s",
                json.dumps([dup["name"] for dup in duplicates], indent=2),
            )
            grouped[path] = duplicates[0]

    return grouped


def create_node_tree(grouped_nodes):
    """ Create a node tree which can be easily mapped into the address space
    from a `grouped_nodes` dict
    """
    tree = {}
    current = tree
    for _path, node in grouped_nodes.items():
        array = node.get("array")
        if array:
            first = array["nodes"][0]
            dirname = first["dirname"]
            basename = first["basename"]
        else:
            dirname = node["dirname"]
            basename = node["basename"]

        for part in dirname:
            sub = current.get(part)
            if sub is None:
                sub = {}
                current[part] = sub
            current = sub
        current[basename] = node
        current = tree
    return tree


def random_array_node_value(node):
    """ Create random values for the given array `node` """
    array = node["array"]
    nodes = array["nodes"]
    length = array["length"]
    array_value = [random_node_value(nodes[i]) for i in range(length)]
    return array_value


def _adjust_value(node, value):
    """ Adjust node accord to the `adjust` information attached to the given
    `node`
    """
    adjust = node.get("adjust")
    if not adjust:
        return value
    return getattr(value, adjust["op"])(adjust["value"])


def random_node_value(node):
    """ Create random value depending on the type and range information,
    attached to the node
    """
    # Assume boolean as default type
    # This is only necessary because `cond` nodes don't provide any
    # type information (because fuck you, that's why!) and they are
    # basically booleans
    data_type = node["type_info"]["data_type"]
    left, right = node.get("value_range") or (-(2 ** 32), 2 ** 32)
    if data_type == "str":
        value = fake.sentence()
    elif data_type == "int":
        value = fake.random_int(min=left, max=right)
        value = _adjust_value(node, value)
    elif data_type == "float":
        value = random.uniform(a=left, b=right)
        value = _adjust_value(node, value)
    elif data_type == "bool":
        value = random.choice([True, False])
    else:
        raise ValueError(f"Unknown data type {data_type!r}")
    return value
