""" YAML loading for the machine model simulation backend of the wackler """

import yaml


def extract_mapped_nodes(node):
    """ Extract relvant nodes for machine model simulation.

    We basically only want mapped nodes
    """
    result = []
    for child in node["nodes"]:
        result += child.get("mappings", [])
        if child.get("nodes"):
            result += extract_mapped_nodes(child) or []
    return result


def _normalize_nodes(model_nodes):
    """ Normalize model nodes nodes """
    result = []
    for node in model_nodes:
        normalized = {
            "expression": node["formula"],
            "type_info": {"data_type": node.get("dtype", "bool")},
        }

        if "rangeMin" in node and "rangeMax" in node:
            normalized["value_range"] = node["rangeMin"], node["rangeMax"]
        else:
            normalized["value_range"] = None

        result.append({**node, **normalized})
    return result


def load(modelfile):
    """ Load model nodes from yaml file """
    model = yaml.safe_load(modelfile)
    model_nodes = extract_mapped_nodes(model)
    return _normalize_nodes(model_nodes)
