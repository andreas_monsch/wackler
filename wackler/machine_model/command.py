# pylint: disable=too-many-arguments,missing-function-docstring
""" Machine model specific wackling"""

import time
import logging
import contextlib as ctx

import opcua

import wackler.machine_model.yaml as yaml
import wackler.machine_model.json as json
import wackler.machine_model.mappy_api as mappy_api
import wackler.machine_model.node_utils as node_utils


logger = logging.getLogger("wackler")


def from_mappy_api(host, username, password):
    """ Retrieve json model from mappy API running on `host` """
    model_name, model_nodes = mappy_api.load(host, username, password)
    return model_name, model_nodes


def from_yaml(modelfile):
    """ Load model nodes from yaml file"""
    return yaml.load(modelfile)


def from_json(modelfile):
    """ Load model nodes from yaml file"""
    return json.load(modelfile)


def simulate_machine_model(host, port, update_interval, model_nodes):
    """ This is the model simulation function

    It updates the variable values every `updaet_interval` seconds. `host`
    and `port` specify under which address the opcua server providing the
    source variables for the mappings of the machine model should be hosted.
    """
    with opcua_server(host, port) as server:
        nodes = node_utils.create_meta_data(model_nodes)
        nodes = node_utils.normalize_nodes(nodes)
        grouped = node_utils.group_nodes_by_path(nodes)
        node_tree = node_utils.create_node_tree(grouped)
        nodes = save_to_address_space(server.get_objects_node(), node_tree)
        while True:
            try:
                time.sleep(update_interval)
                for node in nodes:
                    if node.get("array"):
                        value = node_utils.random_array_node_value(node)
                    else:
                        value = node_utils.random_node_value(node)
                    node["variable"].set_value(value)
            except KeyboardInterrupt:
                logger.info("User requested stop via keyboard interrupt")
                break


@ctx.contextmanager
def opcua_server(host, port):
    endpoint = f"opc.tcp://{host}:{port}"
    logger.info("Starting OPCUA server on %r", endpoint)

    server = opcua.Server()
    server.set_endpoint(f"opc.tcp://{host}:{port}")
    server.start()

    yield server

    logger.info("Shutting down OPCUA server ..")
    server.stop()


def save_to_address_space(objects_node, node_tree):
    """ Save the given `node_tree` to the OPCUA address space and initialize
    variables with random values
    """
    variables = []

    def instantiate_children(parent, tree):
        for node_name, node in tree.items():
            array = node.get("array")
            if array:
                nsidx, node_name = array["nodes"][0]["basename"].split(":", 1)
                node["variable"] = parent.add_variable(
                    int(nsidx), node_name, node_utils.random_array_node_value(node)
                )
                variables.append(node)
            elif node.get("expression"):
                nsidx, node_name = node["basename"].split(":", 1)
                node["variable"] = parent.add_variable(
                    int(nsidx), node_name, node_utils.random_node_value(node)
                )
                variables.append(node)
            else:
                nsidx, node_name = node_name.split(":", 1)
                instantiate_children(parent.add_object(int(nsidx), node_name), node)

    instantiate_children(objects_node, node_tree)
    return variables
