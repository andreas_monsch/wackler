""" JSON loading for the machine model simulation backend of the wackler """

import json

import wackler.machine_model.mappy_api as mappy_api


def load(modelfile):
    """ Load model nodes from json file """
    model = json.load(modelfile)
    return mappy_api.extract_mapped_nodes(model)
