""" Mappy API handling functions """

import requests
from bert_tools.bertc import ApiClient
from bert_tools.oauth import BertOAuthValidator


def _collect_children(node):
    result = []
    for attr in ["set_points", "conditions", "process_values"]:
        children = node.get(attr)
        if node.get(attr):
            if attr == "conditions":
                children = [
                    {**child, "type_info": {"data_type": "bool"}} for child in children
                ]
            result += children

    for child in node.get("sub_equipments", {}):
        result += _collect_children(child)

    return result


def extract_mapped_nodes(model):
    """ Extract relevant nodes from model """
    root = model["result"][0]["root"]
    children = _collect_children(root)
    return children


def load(host, username, password):
    """ Load model from Mappy API running on `host` and return relevant nodes"""
    auth_api_url = f"http://{host}/platform/auth/client"
    basic_auth = (username, password)
    resp = requests.get(auth_api_url, auth=basic_auth)
    resp.raise_for_status()

    for client in resp.json()["clients"]:
        if client["name"] == "wackler":
            wackler = client
            break
    else:
        resp = requests.post(
            auth_api_url,
            auth=basic_auth,
            json={"name": "wackler", "scope": ":", "user_scope": ":"},
        )
        wackler = resp.json()

    validator = BertOAuthValidator(
        client_id=wackler["client_id"],
        client_secret=wackler["client_secret"],
        url=f"http://{host}/platform/auth/",
    )

    rpp_api_client = ApiClient(
        oauth_validator=validator, url=f"http://{host}/app/mappy/api/"
    )
    resp = rpp_api_client.get("model/full")
    resp.raise_for_status()

    json_resp = resp.json()
    model_name = json_resp["result"][0]["root"]["name"]
    return model_name, extract_mapped_nodes(resp.json())
