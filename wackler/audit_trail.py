# pylint: disable=too-many-arguments
""" Audit trail specific code goes in here """

import time
import random
import logging
import datetime as dt
import itertools as it
import pprint as pp

import pytds
import faker


logger = logging.getLogger(__name__)


def audit_trail_command(
    ctx, host, port, database, user, password, login_timeout, wipe_existing
):
    """Wackel sub command that inserts random AutidTrail entries into an MSSQL DB."""
    # Don't specify a DB here - the target database might not yet exist
    with pytds.connect(
        dsn=host, port=port, user=user, password=password, login_timeout=login_timeout
    ) as conn:
        if wipe_existing:
            drop_db(conn, database)

        if not db_exists(conn, database):
            create_db(conn, database)
            create_audit_trail_table(conn, database)

        start_ident_at = get_start_ident(conn, database)

        while True:
            for entry in audit_trail_stream(start_ident_at):
                logger.info("AuditTrail entry: %s", pp.pformat(entry))
                update_audit_trail(conn, database, entry)
                time.sleep(ctx.obj["UPDATE_INTERVAL"])


def get_start_ident(conn, database):
    """Find out at which `ident`, which is the PKEY of the AuditTrail table, to
    start inserting fake AT entries.
    """
    with conn.cursor() as cursor:
        cursor.execute(
            """
            SELECT COALESCE(MAX(ident), 0) + 1
            FROM [%s].dbo.AuditTrail
        """
            % database
        )
        return cursor.fetchone()[0]


def db_exists(conn, database):
    """ Check if the specified `database` exists"""
    with conn.cursor() as cursor:
        cursor.execute(
            """
            IF DB_ID('%s') IS NOT NULL
              SELECT 1 AS db_exists
            ELSE
              SELECT 0 AS db_exists
            """
            % database
        )
        result = cursor.fetchone()
        return bool(result[0])


def drop_db(conn, database):
    """ Drop `database` if it exists"""
    logger.info("Wiping old database + tables")
    old_setting = conn.autocommit
    conn.set_autocommit(True)
    with conn.cursor() as cursor:
        cursor.execute("DROP DATABASE IF EXISTS %s" % database)
    conn.set_autocommit(old_setting)


def create_db(conn, database):
    """ Create `database` """
    logger.info("Creating new database %r", database)
    old_setting = conn.autocommit
    conn.set_autocommit(True)
    with conn.cursor() as cursor:
        cursor.execute("CREATE DATABASE %s" % database)
    conn.set_autocommit(old_setting)


def create_audit_trail_table(conn, database):
    """ Create the `AuditTrail` table in the given `database`"""
    logger.info("Creating 'AuditTrail' table")
    with conn.cursor() as cursor:
        cursor.execute(
            """
        CREATE TABLE [%s].dbo.AuditTrail (
	    ident int IDENTITY(1,1) NOT NULL,
	    [DateTime] nvarchar(40) COLLATE Latin1_General_CI_AS NOT NULL,
	    t1970 int NOT NULL,
	    sId nvarchar(60) COLLATE Latin1_General_CI_AS NULL,
	    idUser nvarchar(40) COLLATE Latin1_General_CI_AS NULL,
	    Priority smallint NULL,
	    Category int NULL,
	    Panel tinyint NULL,
	    TextLang1 nvarchar(1000) COLLATE Latin1_General_CI_AS NULL,
	    TextLang2 nvarchar(1000) COLLATE Latin1_General_CI_AS NULL,
	    TextLang3 nvarchar(1000) COLLATE Latin1_General_CI_AS NULL,
	    ReasonLang1 nvarchar(200) COLLATE Latin1_General_CI_AS NULL,
	    ReasonLang2 nvarchar(200) COLLATE Latin1_General_CI_AS NULL,
	    ReasonLang3 nvarchar(200) COLLATE Latin1_General_CI_AS NULL,
	    CONSTRAINT PK__AuditTra__07D87A79F001357F PRIMARY KEY (ident)
        )
        """
            % database
        )
        conn.commit()


def audit_trail_stream(start_ident_at=1):
    """ Generates an endless stream of fake AuditTrail entries """
    gen = faker.Faker()
    for i in it.count(start_ident_at):
        now = dt.datetime.now()
        yield (
            i,  # ident
            now.strftime("%Y-%m-%d %H:%M:%S"),  # DateTime
            int(now.timestamp()),  # t1970
            random_sId(),  # sId
            random_idUser(),  # idUser
            0,  # Priority
            "NULL",  # Category
            random.randint(a=1, b=3),  # Panel
            gen.text(),
            gen.text(),
            gen.text(),  # TextLang 1-3,
            gen.text(),
            gen.text(),
            gen.text(),  # ReasonLang 1-3,
        )


def update_audit_trail(conn, database, row):
    """ Inserts the given row into the AuditTrail table in the given `database`"""
    params = (database, database) + row
    stmt = (
        """
        SET IDENTITY_INSERT [%s].dbo.AuditTrail ON

        INSERT INTO [%s].dbo.AuditTrail (
            ident,
            DateTime,
            t1970,
            sId,
            idUser,
            Priority,
            Category,
            Panel,
            TextLang1,
            TextLang2,
            TextLang3,
            ReasonLang1,
            ReasonLang2,
            ReasonLang3
        ) VALUES (
            %s, '%s', %s, '%s', '%s', %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s'
        )
        """
        % params
    )
    with conn.cursor() as cursor:
        cursor.execute(stmt)
        conn.commit()


def random_sId():  # pylint: disable= invalid-name
    """ Return random (enough) sId """
    return random.choice(
        [
            "atAuditTrailPrinted",
            "atBackupDbIO",
            "atBatchActChanged",
            "atBatchCreated",
            "atBatchPrinted",
            "atChangeData",
            "atCNTRTotal",
            "atDownloadSetPointNOK",
            "atDownloadSetPointsIO",
            "atElSignTableNotAvailable",
            "atMessagesPrinted",
            "atNewSetPointCopy",
            "atNewUserAdded",
            "atPasswordChanged",
            "atPlcCounterSet",
            "atRemotePrintSettingsChanged",
            "atRemoteReportPrinted",
            "atRestoreDbIO",
            "atSetpointChanged",
            "atSetpointsPrinted",
            "atStart",
            "atStatusChanged",
            "atTableDeleted",
            "atTablesLimited",
            "atTimeTotal",
            "atUpdateImport",
            "atUserLoggedOut",
            "atUserLoggedOutAuto",
            "atUserLoginAsMaster",
            "atUserLoginAsOperator",
            "atUserLoginAsUserAdmin",
            "atUserSettingsChanged",
            "atWrongPassword",
            "bt_ClosedTime",
            "bt_Saved",
            "bt_StartTime",
            "LanguageChangedTo",
        ]
    )


def random_idUser():  # pylint: disable= invalid-name
    """ Return a random (enough) idUser"""
    return random.choice(["Admin", "Service", "SpecialId", "System"])
