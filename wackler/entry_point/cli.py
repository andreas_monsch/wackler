""" This module contains the CLI entry point """

import logging
import time

import click
import requests

import wackler.audit_trail as at
import wackler.machine_model.command as mm

logger = logging.getLogger("wackler")
logger.addHandler(logging.StreamHandler())


@click.group()
@click.option("--update-interval", type=float, default=3.0)
@click.option("--debug", is_flag=True)
@click.pass_context
def wackel(ctx, update_interval, debug):
    """ Main command """
    ctx.ensure_object(dict)
    logger.setLevel("DEBUG" if debug else "INFO")
    ctx.obj["DEBUG"] = debug
    ctx.obj["UPDATE_INTERVAL"] = update_interval


@wackel.command()
@click.option("--host", type=str, default="127.0.0.1")
@click.option("--port", type=int, default=1433)
@click.option("--database", type=str, required=True)
@click.option("--user", type=str, required=True)
@click.option("--password", type=str, required=True)
@click.option("--login-timeout", type=int, default=3)
@click.option(
    "--wipe-existing",
    is_flag=True,
    help="If this flag is set then `database` and all of its tables will dropped an recreated..",
)
@click.pass_context
def audit_trail(
    ctx, host, port, database, user, password, login_timeout, wipe_existing
):
    """ Audit trail sub command which inserts random audit trail entries into
    the `AuditTrail` table in the HMI DB."""
    at.audit_trail_command(
        ctx, host, port, database, user, password, login_timeout, wipe_existing
    )


@wackel.group()
@click.option("--opcua-host", type=str, default="127.0.0.1")
@click.option("--opcua-port", type=int, default=4840)
@click.pass_context
def machine_model(ctx, opcua_host, opcua_port):
    """Wackel sub command that reads a mappy machine model, randomizes node
    values and provides access to the model via OPCUA."""
    ctx.obj["OPCUA_HOST"] = opcua_host
    ctx.obj["OPCUA_PORT"] = opcua_port


@machine_model.command()
@click.option("--modelfile", type=click.File("rb"), default=None)
@click.pass_context
def from_yaml(ctx, modelfile):
    """ Simulate machine model from yaml file """
    logger.info("Loading from YAML file")
    model_nodes = mm.from_yaml(modelfile)
    mm.simulate_machine_model(
        ctx.obj["OPCUA_HOST"],
        ctx.obj["OPCUA_PORT"],
        ctx.obj["UPDATE_INTERVAL"],
        model_nodes,
    )


@machine_model.command()
@click.option("--modelfile", type=click.File("rb"), default=None)
@click.pass_context
def from_json(ctx, modelfile):
    """ Simulate machine model from json file """
    logger.info("Loading from JSON file / Mappy API response dump file")
    model_nodes = mm.from_json(modelfile)
    mm.simulate_machine_model(
        ctx.obj["OPCUA_HOST"],
        ctx.obj["OPCUA_PORT"],
        ctx.obj["UPDATE_INTERVAL"],
        model_nodes,
    )


@machine_model.command()
@click.option("--host", type=str, default="192.168.1.2")
@click.option("--username", type=str, default="admin")
@click.option("--password", type=str, default="pppp")
@click.option(
    "--immortal",
    is_flag=True,
    help="Retry retrieving machine model from Mappy API in case an error occurs.. forever.",
)
@click.pass_context
def from_mappy_api(ctx, host, username, password, immortal):
    """ Simulate machine model loaded from mappy api """
    logger.info("Loading machine model from Mappy API running under %r", host)

    model_nodes = _load_mappy_model_from_api(
        host, username, password, ctx.obj["UPDATE_INTERVAL"], immortal
    )

    mm.simulate_machine_model(
        ctx.obj["OPCUA_HOST"],
        ctx.obj["OPCUA_PORT"],
        ctx.obj["UPDATE_INTERVAL"],
        model_nodes,
    )


def _load_mappy_model_from_api(
    host, username, password, update_interval, immortal=False
):
    while True:
        try:
            model_name, model_nodes = mm.from_mappy_api(host, username, password)
            logger.info("Successfully retrieved machine model (%r) from Mappy API", model_name)
            break
        except requests.HTTPError as exc:
            logger.warning(
                "Failed to retrieve machine model from Mappy API - details: %r ", exc
            )

            if not immortal:
                raise

        logger.info("Retrying in %rs", update_interval)
        time.sleep(update_interval)

    return model_nodes


def main():
    """ Main function """
    wackel()  # pylint: disable=no-value-for-parameter
