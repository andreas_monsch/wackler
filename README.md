# Wat this?

This is wackler (german for wiggler or wobbler) a command line tool which allows its user, in the broadest sense,
to "fake" values for various use cases. So far backends for two use cases are supported, namely a backend for
random `AuditTrail` entry generation and a backend for`Machine Model` simulation, both are relevant
for the manual testing of the RPP Machine Modeler (also known as Mappy).


## AuditTrail Backend
This backend requires a running MS SQL Server instance (a corresp. docker-compose.yml can be found
under ./mssql) and creates DB and the `AuditTrail` table and inserts random `AuditTrail` rows.
This then triggers stuff in Mappy which eventually results in Mappy firing `AuditTrailUpdateEvents`.

Wackler is of course able to append rows to an existing `AuditTrail` table, it automatically discovers
the last `indent` (ident is HMI-retard-speak for ID) and starts from there.

## Machine Model Backend
The wackler supports also machine model simulation. This basically reads a mappy machine model, either provided
by the mappy API or directly via a yaml file, parses it and creates a information model based on the source nodes,
used for the variable mapping. It then exposes this information model via an OPCUA server and updates the corresponding
variables with random values BUT respects the value ranges configured in the machine model.

Note: The mapping feature of mappy allows complex expressions which are basically only limited by Pythons
capabilities. Note that wackler does only support a very limited range of expressions. It might very well be that
the wacklers capabilities need to be updated (probably by you!) in this regard.

# How to setup locally?

```bash
python -m venv venv
. ./venv/bin/activate
pip install -U
pip install -r requirements.txt
pip install -r requirements-dev.txt
pip install -e .
```

# How to run locally?

## AuditTrail backend
Fire up the docker-compose project under `./mssql/docker-compose.yml` in order to start your ms sql server.


Note that if no `--host` and `--port` are given, Wackler assumes your DB is running under `localhost:1433`.


```bash
wackler audit-trail         \  # Use the audit trail wackel backend
	    --user=<hans>       \  # Username
		--password=<s3cre7> \  # Password
		--database=<dbname> \  # Database name
		--wipe-existing        # Drop existing DB with <dbname>, and `AuditTrail` table and recreate
```

### Real world example

```bash
wackler audit-trail --user=SA --password='12345678!abcdefgh' --database=BD460XXX --wipe-existing
```

### Usage

```
Usage: wackler audit-trail [OPTIONS]                                                
                                                                                    
Options:                                                                            
  --host TEXT                                                                       
  --port INTEGER                                                                    
  --database TEXT          [required]                                               
  --user TEXT              [required]                                               
  --password TEXT          [required]                                               
  --login-timeout INTEGER                                                           
  --wipe-existing          If this flag is set then `database` and all of its       
                           tables will dropped an recreated..                       
                                                                                    
  --help                   Show this message and exit.                              
```

## Machine model backend

Note that the `from-mappy-api` sub command assumes a locally running vRPP setup with a mappy API running on 192.168.1.2. The
`--opcua-host` option set to `"0.0.0.0"` allows you to use the simulator as a data source from mappy - using `"127.0.0.1"`
will result in the server refusing your connection.

```bash
wackler machine-model    \ # Use machine-model simulation backend
  --opcua-host "0.0.0.0" \ # Allow connecting to the provided OPCUA Host from anywhere
  from-mappy-api           # Load the machine model directly from mappy
```

# Usage

```
Usage: wackler machine-model [OPTIONS] COMMAND [ARGS]...

  Wackel sub command that reads a mappy machine model, randomizes nodes
  values and provides access to the model via OPCUA.

Options:
  --opcua-host TEXT
  --opcua-port INTEGER
  --help                Show this message and exit.

Commands:
  from-mappy-api
  from-yaml
```


## ToDos

- audit-trail: Suppress verbose logging from modules other than wackler.
- machine-model: Add support for `not` expressions

