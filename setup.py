import setuptools

setuptools.setup(
    name="wackler", # Replace with your own username
    version="0.0.1",
    author_email="leet-haxx0r-amo@who-needs-a-domain-lol?.amo",
    packages=setuptools.find_packages(),
    install_requires=[
        "click",
        "python-tds",
        "Faker",
        "pyyaml",
        "opcua",
    ],
    entry_points={
        'console_scripts': [
            'wackler = wackler.entry_point.cli:main',
        ],
    }
)
