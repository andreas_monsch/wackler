FROM python:3.8-slim-buster

RUN apt-get update
RUN pip install --upgrade pip

ENV TZ Europe/Berlin

WORKDIR /app
COPY . /app

RUN pip wheel --no-deps --no-index -w /cache/wheels-dep -r requirements.txt
RUN pip install --no-deps --no-index /cache/wheels-dep/*.whl
RUN pip install --no-deps --no-index -r requirements-dev.txt
RUN pip install .
