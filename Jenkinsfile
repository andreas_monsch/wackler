gcloudDefineOtfAgents()

pipeline {
    agent any
    stages {
        stage('Setup OTF Agent') {
          steps {
            gcloudCreateOtfAgents default: "1"
          }
        }
        stage('Unit tests') {
            agent { label "${OTF_DEFAULT_LABEL}" }
            steps {
                sh 'pip install pytest pytest-cov'
                sh 'PYTHONPATH=./ pytest --junitxml=./test_report.xml -vv --cov ajpi --cov tests --cov-report xml'
                sh 'ls -al'
            }
            post {
                always {
                    catchError{
                        junit 'test_report.xml'
                    }
                    archiveArtifacts artifacts: 'test_report.xml', fingerprint: true
                    archiveArtifacts artifacts: 'coverage.xml', fingerprint: true
                    step([$class: 'CoberturaPublisher', coberturaReportFile: 'coverage.xml'])
                }
            }
        }
	stage('Build docker image') {
	    agent {
		docker {
		    image 'rommelag/appify'
		    args '--privileged'
		    alwaysPull true
		}
	    }
	    steps {
		script {
		    // Overwrite BUILD file for appify
		    generateBuildNumber()
		    dockerBuild "rommelag/wackler:${env.DOCKER_BUILD_TAG}", push: true
		    sh 'appify'
		}
	    }
	    post {
		success {
		    genericUpload(['*.app.tar'], 'generic-local/wackler/')
		}
	    }
	}
    }
    post {
        always {
            gcloudDeleteOtfAgents()
        }
    }
}
